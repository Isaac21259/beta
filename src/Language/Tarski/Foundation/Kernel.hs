module Language.Tarski.Foundation.Kernel where

import Control.Monad.Hrolog
import Language.Tarski.Term
import Language.Tarski.Foundation.Judgements
import Control.Monad

synthesize :: SyntacticTerm -> [SyntacticTerm]
synthesize t = evalHrolog $ do
  tm <- free
  tis Nil tm (intoH t) (HTuple Nil Nil)
  allNoMData tm
  fromH tm

typecheck :: SyntacticTerm -> SyntacticTerm -> [Bool]
typecheck tm t = evalHrolog $ do
  es <- free
  tis Nil (intoH tm) (intoH t) es
  return True

pushedCtx :: Ctx s -> Ctx s -> Pred s
pushedCtx p c = do
  pVars <- free
  pTypes <- free
  zipped p pVars pTypes
  cVars <- free
  cTypes <- free
  zipped c cVars cTypes
  vars <- free
  zipped vars pVars cVars
  predTupleHList (\p' c' -> p' === Succ c') vars
  types <- free
  zipped types pTypes cTypes
  predTupleHList (pushed Zero) types

tis :: TIS s
tis ctx tm t ds = msum $ fmap (\r -> r ctx tm t ds) [bottomForm, bottomElim, topForm, topIntro, twoForm, twoIntro1, twoIntro2, universeTower, piIntro, piForm, piElim, varIntro]

bottomForm :: TIS s
bottomForm _ tm t _ = do
  m1 <- free
  tm === uBottom m1
  m2 <- free
  t === uUniverse m2 Zero

bottomElim :: TIS s
bottomElim ctx tm _ ds = do
  m1 <- free
  b <- free
  tm === uAbsurd m1 b
  m2 <- free
  tis ctx b (uBottom m2) ds

topForm :: TIS s
topForm _ tm t _ = do
  m1 <- free
  tm === uTop m1
  m2 <- free
  t === uUniverse m2 Zero

topIntro :: TIS s
topIntro _ tm t _ = do
  m1 <- free
  tm === uUnit m1
  m2 <- free
  t === uTop m2

-- Missing ElTop

twoForm :: TIS s
twoForm _ tm t _ = do
  m1 <- free
  tm === uTwo m1
  m2 <- free
  t === uUniverse m2 Zero

twoIntro1 :: TIS s
twoIntro1 _ tm t _ = do
  m1 <- free
  tm === uTt m1
  m2 <- free
  t === uTwo m2

twoIntro2 :: TIS s
twoIntro2 _ tm t _ = do
  m1 <- free
  tm === uFf m1
  m2 <- free
  t === uTwo m2

universeTower :: TIS s
universeTower ctx tm t ds = do
  i <- free
  m1 <- free
  t === uUniverse m1 i
  j <- free
  k <- free
  hNatPlus i j (Succ k)
  m2 <- free
  tis ctx tm (uUniverse m2 j) ds

-- Missing ElTwo

-- Pi's are currently just function types
piForm :: TIS s
piForm ctx tm t ds = do
  l <- free
  r <- free
  m1 <- free
  u <- free
  t === uUniverse m1 u
  m2 <- free
  tm === uPi m2 l r
  tis ctx l t ds
  ctx' <- free
  pushedCtx ctx' ctx
  tis (Cons (HTuple Zero l) ctx) r t ds

piIntro :: TIS s
piIntro ctx tm t ds = do
  b <- free
  m1 <- free
  tm === uLambda m1 b
  l <- free
  r <- free
  m2 <- free
  t === uPi m2 l r
  ctx' <- free
  pushedCtx ctx' ctx
  tis (Cons (HTuple Zero l) ctx') b r ds

piElim :: TIS s
piElim ctx tm t ds = do
  f <- free
  a <- free
  m1 <- free
  tm === uApply m1 f a
  at <- free
  tis ctx a at ds
  m2 <- free
  tis ctx f (uPi m2 at t) ds

varIntro :: TIS s
varIntro ctx tm t ds = do
  m1 <- free
  v <- free
  tm === uVar m1 v
  inHList (HTuple v t) ctx
