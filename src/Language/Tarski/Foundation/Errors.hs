module Language.Tarski.Foundation.Errors
  ( TypeError (..)
  , TypeWarning (..)
  ) where

data TypeError = TypeError deriving Show
data TypeWarning = TypeWarning deriving Show
