{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Language.Tarski.Foundation.Judgements where
import Control.Monad.Hrolog
import Language.Tarski.Foundation.Errors
import Language.Tarski.Term
import Control.Monad

type Ctx s = HList s (HTuple s (HNat s) (UTerm s))

type Diagnostics = ([TypeError], [TypeWarning])
type UDiagnostics s = HTuple s (HList s (HWrapper s TypeError)) (HList s (HWrapper s TypeWarning)) 

type TIS s = Ctx s -> UTerm s -> UTerm s -> UDiagnostics s -> Pred s

uBottom :: UMetadata s -> UTerm s
uBottom m = Other (BottomF m)

uAbsurd :: UMetadata s -> UTerm s -> UTerm s
uAbsurd m b = Other (AbsurdF m b)

uTop :: UMetadata s -> UTerm s
uTop m = Other (TopF m)

uUnit :: UMetadata s -> UTerm s
uUnit m = Other (UnitF m)

uElTop :: UMetadata s -> UTerm s -> UTerm s
uElTop m b = Other (ElTopF m b)

uTwo :: UMetadata s -> UTerm s
uTwo m = Other (TwoF m)

uTt :: UMetadata s -> UTerm s
uTt m = Other (TtF m)

uFf :: UMetadata s -> UTerm s
uFf m = Other (FfF m)

uElTwo :: UMetadata s -> UTerm s -> UTerm s -> UTerm s
uElTwo m l r = Other (ElTwoF m l r)

uUniverse :: UMetadata s -> HNat s -> UTerm s
uUniverse m t = Other (UniverseF m t)

uPi :: UMetadata s -> UTerm s -> UTerm s -> UTerm s
uPi m l r = Other (PiF m l r)

uLambda :: UMetadata s -> UTerm s -> UTerm s
uLambda m b = Other (LambdaF m b)

uApply :: UMetadata s -> UTerm s -> UTerm s -> UTerm s
uApply m l r = Other (ApplyF m l r)

uSigma :: UMetadata s -> UTerm s -> UTerm s -> UTerm s
uSigma m l r = Other (SigmaF m l r)

uCons :: UMetadata s -> UTerm s -> UTerm s -> UTerm s
uCons m l r = Other (ConsF m l r)

uElSigma :: UMetadata s -> UTerm s -> UTerm s
uElSigma m b = Other (ElSigmaF m b)

uVar :: UMetadata s -> HNat s -> UTerm s
uVar m t = Other (VarF m t)

uExternal :: UMetadata s -> UName s -> UTerm s
uExternal m n = Other (ExternalF m n)

uMetadata :: UMetadata s -> UTerm s -> Hrolog s ()
uMetadata m tm = msum [ tm === uBottom m
                      , free >>= \b -> tm === uAbsurd m b
                      , tm === uTop m
                      , tm === uUnit m
                      , free >>= \b -> tm === uElTop m b
                      , tm === uTwo m
                      , tm === uTt m
                      , tm === uFf m
                      , free >>= \l -> free >>= \r -> tm === uElTwo m l r
                      , free >>= \u -> tm === uUniverse m u
                      , free >>= \l -> free >>= \r -> tm === uPi m l r
                      , free >>= \b -> tm === uLambda m b
                      , free >>= \l -> free >>= \r -> tm === uApply m l r
                      , free >>= \l -> free >>= \r -> tm === uSigma m l r
                      , free >>= \l -> free >>= \r -> tm === uCons m l r
                      , free >>= \b -> tm === uElSigma m b
                      , free >>= \v -> tm === uVar m v
                      , free >>= \n -> tm === uExternal m n
                      ]

allChildren :: (UTerm s -> Hrolog s ()) -> UTerm s -> Hrolog s ()
allChildren f tm = f tm >> msum [ free >>= \m -> tm === uBottom m
                                , free >>= \m -> free >>= \b -> tm === uAbsurd m b >> allChildren f b
                                , free >>= \m -> tm === uTop m
                                , free >>= \m -> tm === uUnit m
                                , free >>= \m -> free >>= \b -> tm === uElTop m b >> allChildren f b
                                , free >>= \m -> tm === uTwo m
                                , free >>= \m -> tm === uTt m
                                , free >>= \m -> tm === uFf m
                                , free >>= \m -> free >>= \l -> free >>= \r -> tm === uElTwo m l r >> allChildren f l >> allChildren f r
                                , free >>= \m -> free >>= \u -> tm === uUniverse m u
                                , free >>= \m -> free >>= \l -> free >>= \r -> tm === uPi m l r >> allChildren f l >> allChildren f r
                                , free >>= \m -> free >>= \b -> tm === uLambda m b >> allChildren f b
                                , free >>= \m -> free >>= \l -> free >>= \r -> tm === uApply m l r >> allChildren f l >> allChildren f r
                                , free >>= \m -> free >>= \l -> free >>= \r -> tm === uSigma m l r >> allChildren f l >> allChildren f r
                                , free >>= \m -> free >>= \l -> free >>= \r -> tm === uCons m l r >> allChildren f l >> allChildren f r
                                , free >>= \m -> free >>= \b -> tm === uElSigma m b >> allChildren f b
                                , free >>= \m -> free >>= \v -> tm === uVar m v
                                , free >>= \m -> free >>= \n -> tm === uExternal m n
                                ]

allNoMData :: UTerm s -> Hrolog s ()
allNoMData tm = msum [ tm === uBottom HNothing
                     , free >>= \b -> tm === uAbsurd HNothing b >> allNoMData b
                     , tm === uTop HNothing
                     , tm === uUnit HNothing
                     , free >>= \b -> tm === uElTop HNothing b >> allNoMData b
                     , tm === uTwo HNothing
                     , tm === uTt HNothing
                     , tm === uFf HNothing
                     , free >>= \l -> free >>= \r -> tm === uElTwo HNothing l r >> allNoMData l >> allNoMData r
                     , free >>= \u -> tm === uUniverse HNothing u
                     , free >>= \l -> free >>= \r -> tm === uPi HNothing l r >> allNoMData l >> allNoMData r
                     , free >>= \b -> tm === uLambda HNothing b >> allNoMData b
                     , free >>= \l -> free >>= \r -> tm === uApply HNothing l r >> allNoMData l >> allNoMData r
                     , free >>= \l -> free >>= \r -> tm === uSigma HNothing l r >> allNoMData l >> allNoMData r
                     , free >>= \l -> free >>= \r -> tm === uCons HNothing l r >> allNoMData l >> allNoMData r
                     , free >>= \b -> tm === uElSigma HNothing b >> allNoMData b
                     , free >>= \v -> tm === uVar HNothing v
                     , free >>= \n -> tm === uExternal HNothing n
                     ]

pushed :: HNat s -> UTerm s -> UTerm s -> Pred s
pushed n tm tm' = msum [ do
                           m <- free
                           tm === uBottom m
                           m' <- free
                           tm' === uBottom m'
                       , do
                           b <- free
                           m <- free
                           tm === uAbsurd m b
                           b' <- free
                           m' <- free
                           tm' === uAbsurd m' b'
                           pushed n b b'
                       , do
                           m <- free
                           tm === uTop m
                           m' <- free
                           tm' === uTop m'
                       , do
                           m <- free
                           tm === uUnit m
                           m' <- free
                           tm' === uUnit m'
                       , do
                           b <- free
                           m <- free
                           tm === uElTop m b
                           b' <- free
                           m' <- free
                           tm' === uElTop m' b'
                           pushed n b b'
                       , do
                           m <- free
                           tm === uTwo m
                           m' <- free
                           tm' === uTwo m'
                       , do
                           m <- free
                           tm === uTt m
                           m' <- free
                           tm' === uTt m'
                       , do
                           m <- free
                           tm === uFf m
                           m' <- free
                           tm' === uFf m'
                       , do
                           l <- free
                           r <- free
                           m <- free
                           tm === uElTwo m l r
                           m' <- free
                           l' <- free
                           r'<- free
                           tm' === uElTwo m' l' r'
                           pushed n l l'
                           pushed n r r'
                       , do
                           u <- free
                           m <- free
                           tm === uUniverse m u
                           u' <- free
                           m' <- free
                           tm' === uUniverse m' u'
                           u === u'
                       , do
                           l <- free
                           r <- free
                           m <- free
                           tm === uPi m l r
                           m' <- free
                           l' <- free
                           r'<- free
                           tm' === uPi m' l' r'
                           pushed n l l'
                           pushed (Succ n) r r'
                       , do
                           b <- free
                           m <- free
                           tm === uLambda m b
                           b' <- free
                           m' <- free
                           tm' === uLambda m' b'
                           pushed (Succ n) b b'
                       , do
                           l <- free
                           r <- free
                           m <- free
                           tm === uApply m l r
                           m' <- free
                           l' <- free
                           r'<- free
                           tm' === uApply m' l' r'
                           pushed n l l'
                           pushed n r r'
                       , do
                           l <- free
                           r <- free
                           m <- free
                           tm === uSigma m l r
                           m' <- free
                           l' <- free
                           r'<- free
                           tm' === uSigma m' l' r'
                           pushed n l l'
                           pushed (Succ n) r r'
                       , do
                           l <- free
                           r <- free
                           m <- free
                           tm === uCons m l r
                           m' <- free
                           l' <- free
                           r'<- free
                           tm' === uCons m' l' r'
                           pushed n l l'
                           pushed n r r'
                       , do
                           b <- free
                           m <- free
                           tm === uElSigma m b
                           b' <- free
                           m' <- free
                           tm' === uElSigma m' b'
                           pushed n b b'
                       , do
                           v <- free
                           m <- free
                           tm === uVar m v
                           v' <- free
                           m' <- free
                           tm' === uVar m' v'
                           x <- free
                           hNatPlus v n x
                           v' === Succ v
                       , do
                           v <- free
                           m <- free
                           tm === uVar m v
                           v' <- free
                           m' <- free
                           tm' === uVar m' v'
                           x <- free
                           hNatPlus n v (Succ x)
                           v' === v
                       , do
                           e <- free
                           m <- free
                           tm === uExternal m e
                           e' <- free
                           m' <- free
                           tm' === uExternal m' e'
                           e === e'
                       ]
