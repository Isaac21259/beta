{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeFamilies #-}

module Language.Tarski.Vernacular.Definition
  ( ModuleName
  , Name (..)
  , Module (..)
  , Definition (..)
  , Parser
  , runModuleParser
  , root
  , SyntacticTerm
  ) where

import Control.Monad.State
import Data.Functor.Identity
import Data.List
import Data.ReproducibleSource
import qualified Data.Text as T
import Data.Void
import Language.Tarski.Foundation.ExtendableTerm
import Text.Megaparsec hiding (State)
import Text.Megaparsec.Char
import Control.Monad.Reader

type ModuleName = [T.Text]

data Name = Name {inModule :: ModuleName, name :: T.Text}

data Module = Module {moduleName :: ModuleName, exports :: [T.Text], definitions :: [Definition], imports :: [Module]}

data Definition = Definition ReproducibleSource Name SyntacticTerm SyntacticTerm

data PState = PState [Definition] [T.Text] [Module]

type Parser a = ParsecT Void T.Text (ReaderT [T.Text] (State PState)) a

runModuleParser :: Parser a -> FilePath -> T.Text -> Either (ParseErrorBundle T.Text Void) a
runModuleParser p f s = evalState (runReaderT (runParserT p f s) []) (PState [] [] [])

withLocal :: T.Text -> Parser a -> Parser a
withLocal t p = do
  lift $ modify $ \(PState ds ls im) -> PState ds (t:ls) im
  a <- p
  lift $ modify $ \(PState ds (_:ls) im) -> PState ds ls im
  return a

setDefinition :: Parser Definition -> Parser Definition
setDefinition p = do
  d <- p
  lift $ modify $ \(PState ds ls im) -> PState (d:ds) ls im
  return d

root :: Parser Module
root = do
  space
  _ <- char '('
  space
  _ <- string "module"
  space1
  n <- sepBy (T.pack <$> some letterChar) (char '.')
  space
  _ <- char '('
  space
  e <- sepBy (T.pack <$> some letterChar) space1
  space
  _ <- char ')'
  space
  _ <- char ')'
  space
  _ <- many (define n <* space)
  space
  eof
  PState ds _ is <- lift $ get
  return $ Module n e ds is

define :: ModuleName -> Parser Definition
define mn = setDefinition $ between "(" ")" $ do
  space
  p <- getSourcePos
  d <- string "define"
  space1
  n <- some letterChar
  space1
  tm <- term
  space1
  t <- term
  space
  return $ Definition (ReproducibleSource [NonElideable p d, SourceHole, SourceHole]) (Name mn $ T.pack n) tm t

term :: Parser SyntacticTerm
term = choice
       [ nilAryTermP "Bottom" BottomX
       , nilAryTermP "Top" TopX
       , nilAryTermP "unit" UnitX
       , nilAryTermP "Bool" BoolX
       , nilAryTermP "#t" TtX
       , nilAryTermP "#f" FfX
       , nilAryTermP "U" TypeX
       , nilAryTermP "#f" FfX
       , oneAryTermP "absurd" AbsurdX
       , oneAryTermP "elTop" ElTopX
       , oneAryTermP "elBool" ElBoolX
       , oneAryTermP "elSigma" ElSigmaX
       , lambdaP
       , piP
       , applyP
       , sigmaP
       , consP
       , varP
       , externalP
       ]

nilAryTermP :: T.Text -> (ReproducibleSource -> SyntacticTerm) -> Parser SyntacticTerm
nilAryTermP t c = do
  p <- getSourcePos
  b <- string t
  return $ c (ReproducibleSource [NonElideable p b])

oneAryTermP :: T.Text -> (ReproducibleSource -> SyntacticTerm -> SyntacticTerm) -> Parser SyntacticTerm
oneAryTermP t c = between "(" ")" $ do
  space
  p <- getSourcePos
  b <- string t
  _ <- space1
  tm <- term
  space
  return $ c (ReproducibleSource [NonElideable p b]) tm

lambdaP :: Parser SyntacticTerm
lambdaP = between "(" ")" $ do
  space
  p <- getSourcePos
  b <- string "lambda"
  _ <- space1
  v <- between "(" ")" $ some alphaNumChar
  _ <- space1
  tm <- withLocal (T.pack v) term
  space
  return $ LambdaX (T.pack v, ReproducibleSource [NonElideable p b]) tm

piP :: Parser SyntacticTerm
piP = between "(" ")" $ do
  space
  p <- getSourcePos
  b <- string "Pi"
  _ <- space1
  _ <- char '('
  v <- some alphaNumChar
  _ <- space1
  tm1 <- term
  _ <- char ')'
  _ <- space1
  tm2 <- withLocal (T.pack v) term
  space
  return $ PiX (T.pack v, ReproducibleSource [NonElideable p b]) tm1 tm2

applyP :: Parser SyntacticTerm
applyP = between "(" ")" $ do
  space
  p <- getSourcePos
  tm1 <- term
  _ <- space1
  tm2 <- term
  space
  return $ ApplyX (ReproducibleSource []) tm1 tm2

sigmaP :: Parser SyntacticTerm
sigmaP = between "(" ")" $ do
  space
  p <- getSourcePos
  b <- string "Sigma"
  _ <- space1
  _ <- char '('
  v <- some alphaNumChar
  _ <- space1
  tm1 <- term
  _ <- char ')'
  _ <- space1
  tm2 <- withLocal (T.pack v) term
  space
  return $ SigmaX (T.pack v, ReproducibleSource [NonElideable p b]) tm1 tm2

consP :: Parser SyntacticTerm
consP = between "(" ")" $ do
  space
  p <- getSourcePos
  _ <- space1
  b <- string "cons"
  tm1 <- term
  _ <- space1
  tm2 <- term
  space
  return $ ConsX (ReproducibleSource [NonElideable p b]) tm1 tm2

varP :: Parser SyntacticTerm
varP = do
  v <- some alphaNumChar
  PState _ ls _ <- lift get
  case elemIndex (T.pack v) ls of
    Just i -> return $ VarX (T.pack v, i)
    Nothing -> fail "The identifier is not a local variable"

externalP :: Parser SyntacticTerm
externalP = do
  v <- some alphaNumChar
  PState ds _ _ <- lift get
  let  check (Definition _ t _ _) = T.pack v == name t
  case filter check ds of 
    [] -> fail "The identifier is not defined"
    Definition _ t _ _:_ -> return $ ExternalX (T.pack v, t)

data SyntacticTermData
type SyntacticTerm = TermX SyntacticTermData

type instance XBottom SyntacticTermData = ReproducibleSource
type instance XAbsurd SyntacticTermData = ReproducibleSource
type instance XTop SyntacticTermData = ReproducibleSource
type instance XUnit SyntacticTermData = ReproducibleSource
type instance XElTop SyntacticTermData = ReproducibleSource
type instance XBool SyntacticTermData = ReproducibleSource
type instance XTt SyntacticTermData = ReproducibleSource
type instance XFf SyntacticTermData = ReproducibleSource
type instance XElBool SyntacticTermData = ReproducibleSource
type instance XType SyntacticTermData = ReproducibleSource
type instance XPi SyntacticTermData = (T.Text, ReproducibleSource)
type instance XLambda SyntacticTermData = (T.Text, ReproducibleSource)
type instance XApply SyntacticTermData = ReproducibleSource
type instance XSigma SyntacticTermData = (T.Text, ReproducibleSource)
type instance XCons SyntacticTermData = ReproducibleSource
type instance XElSigma SyntacticTermData = ReproducibleSource
type instance XVar SyntacticTermData = (T.Text, Int)
type instance XExternal SyntacticTermData = (T.Text, Name)
type instance XExtend SyntacticTermData = ()
