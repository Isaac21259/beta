{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE UndecidableInstances #-}

module Control.Monad.Hrolog
  ( Hrolog
  , Pred
  , newHRef
  , Var
  , Free (..)
  , Unify (..)
  , (===)
  , evalHrolog
  , HConvert (..)
  , variable
  , HNat (Zero, Succ)
  , hNatPlus
  , HList (Nil, Cons)
  , allElements
  , zipped
  , inHList
  , appendHList
  , predTupleHList
  , Fix (..)
  , HFix (..)
  , HMaybe (HJust, HNothing)
  , HTuple (HTuple)
  , HWrapper (Wrapped)
  , HChar
  , HString
  ) where

import Control.Monad.Trans.Class
import Control.Monad.ST
import Data.STRef
import Control.Monad
import Control.Applicative
import Control.Monad.Logic
import GHC.Natural
import qualified Data.Text as T

-- Implementation of "Typed Logical Variables in Haskell" by Koen Claessen and Peter Ljunglöf

newtype HrologT m a = HrologT {runHrologT :: forall ans. Int -> (a -> m (Logic ans)) -> m (Logic ans)}

instance Functor (HrologT m) where
  fmap f h = HrologT $ \d c -> runHrologT h d (c . f)

instance Applicative (HrologT m) where
  pure x = HrologT $ \_ c -> c x
  f <*> x = HrologT $ \d c -> runHrologT f d (\g -> runHrologT x d (c . g))

instance Monad (HrologT m) where
  h >>= f = HrologT $ \d c -> runHrologT h d (\x -> runHrologT (f x) d c)

instance MonadFail m => MonadFail (HrologT m) where
  fail _ = HrologT $ \_ _ -> return empty

instance Monad m => Alternative (HrologT m) where
  empty = HrologT $ \_ _ -> return empty
  l <|> r = HrologT $ \d c -> if d == 0 then return empty else do
    l' <- runHrologT l (d - 1) c
    r' <- runHrologT r (d - 1) c
    return $ l' <|> r'

instance Monad m => MonadPlus (HrologT m)

instance MonadTrans HrologT where
  lift m = HrologT $ \_ c -> m >>= \a -> c a

type Hrolog s = HrologT (ST s)

type Pred s = Hrolog s ()

newHRef :: a -> Hrolog s (STRef s a)
newHRef = lift . newSTRef

readHRef :: STRef s a -> Hrolog s a
readHRef = lift . readSTRef

writeHRef :: STRef s a -> a -> Hrolog s ()
writeHRef r a = HrologT $ \_ c -> do
  a' <- readSTRef r
  writeSTRef r a
  ans <- c ()
  writeSTRef r a'
  return ans

type Var s a = STRef s (Maybe a)

class Free s a | a -> s where
  free :: Hrolog s a
  isVar :: a -> Maybe (Var s a)

class Unify s a where
  unify :: a -> a -> Hrolog s ()

unifyVar :: (Free s a, Unify s a) => Var s a -> a -> Hrolog s ()
unifyVar r a = do
  mb <- readHRef r
  case mb of
    Nothing-> writeHRef r (Just a)
    Just b -> a === b

(===) :: (Free s a, Unify s a) => a -> a -> Hrolog s ()
a === b = case (isVar a, isVar b) of
            (Just v1, Just v2) | v1 == v2 -> return ()
            (Just v, _) -> unifyVar v b
            (_, Just v) -> unifyVar v a
            _ -> unify a b

evalHrolog :: (forall s. Hrolog s a) -> [a]
evalHrolog m = concat $ for (\l n -> let l' = drop n l in (l', length l)) 0 $ fmap evalWith [1..]
  where evalWith n = observeAll $ runST $ runHrologT m n (return . return)
        for :: (a -> s -> (b, s)) -> s -> [a] -> [b]
        for _ _ [] = []
        for f s (x:xs) = let (x', s') = f x s in x':for f s' xs

class HConvert s a b | b -> s where
  intoH :: a -> b
  fromH :: b -> Hrolog s a

variable :: HConvert s b a => Var s a -> Hrolog s b
variable v = do
  v' <- readHRef v
  maybe mzero fromH v'

data HNat s = Zero
            | Succ (HNat s)
            | VarH (Var s (HNat s))

instance Free s (HNat s) where
  free = VarH <$> newHRef Nothing
  isVar (VarH v) = Just v
  isVar _ = Nothing

instance Unify s (HNat s) where
  Zero `unify` Zero = return ()
  (Succ l) `unify` (Succ r) = l === r
  _ `unify` _ = mzero

instance HConvert s Natural (HNat s) where
  intoH 0 = Zero
  intoH n = Succ $ intoH $ n - 1
  fromH Zero = return 0
  fromH (Succ n) = (+ 1) <$> fromH n
  fromH (VarH v) = variable v

instance Show (HNat s) where
  show (Succ a) = "(Succ " ++ show a ++ ")"
  show Zero = "0"
  show (VarH _) = "<var>"

hNatPlus :: HNat s -> HNat s -> HNat s -> Hrolog s ()
hNatPlus r a b = base <|> recur
  where base = do
          b === Zero
          r === a
        recur = do
          b' <- free
          b === Succ b'
          hNatPlus r (Succ a) b'

data HList s a = VarL (Var s (HList s a))
               | Nil
               | Cons a (HList s a)

instance Free s (HList s a) where
  free = VarL <$> newHRef Nothing
  isVar (VarL v) = Just v
  isVar _ = Nothing

instance (Unify s a, Free s a) => Unify s (HList s a) where
  Nil `unify` Nil = return ()
  (Cons la ld) `unify` (Cons ra rd) = la === ra >> ld === rd
  _ `unify` _ = mzero

instance (HConvert s a b) => HConvert s [a] (HList s b) where
  intoH [] = Nil
  intoH (x:xs) = Cons (intoH x) (intoH xs)
  fromH Nil = return []
  fromH (Cons x xs) = liftM2 (:) (fromH x) (fromH xs)
  fromH (VarL v) = variable v

instance Show a => Show (HList s a) where
  show l = "[" ++ go l ++ "]"
    where go (Cons x xs) = show x ++ ", " ++ go xs
          go Nil = ""
          go (VarL _) = "... <var>"

allElements :: (Unify s a, Free s a) => (a -> Pred s) -> HList s a -> Pred s
allElements f l = l === Nil <|> do
  x <- free
  xs <- free
  l === Cons x xs
  f x
  allElements f xs

zipped :: (Unify s a, Free s a, Unify s b, Free s b) => HList s (HTuple s a b) -> HList s a -> HList s b -> Pred s
zipped r a b = (a === Nil >> b === Nil >> r === Nil) <|> do
  a' <- free
  as <- free
  a === Cons a' as
  b' <- free
  bs <- free
  b === Cons b' bs
  rs <- free
  zipped rs as bs
  r === Cons (HTuple a' b') rs

inHList :: (Unify s a, Free s a) => a -> HList s a -> Pred s
inHList a l = do
  x <- free
  xs <- free
  l === Cons x xs
  x === a <|> inHList a xs

appendHList :: (Unify s a, Free s a) => HList s a -> HList s a -> HList s a -> Pred s
appendHList r a b = (a === Nil >> r === b) <|> do
  a' <- free
  as <- free
  a === Cons a' as
  r' <- free
  rs <- free
  a' === r'
  appendHList rs as b

predTupleHList :: (Unify s a, Free s a, Unify s b, Free s b) =>  (a -> b -> Pred s) -> HList s (HTuple s a b) -> Pred s
predTupleHList p l = (l === Nil) <|> do
  a <- free
  b <- free
  ls <- free
  l === Cons (HTuple a b) ls
  p a b
  predTupleHList p ls

newtype Fix f = Fix { unfix :: f (Fix f) }

instance Show (f (Fix f)) => Show (Fix f) where
  show (Fix f) = show f

data HFix s f = HVar (Var s (HFix s f))
              | Other (f (HFix s f))

instance Free s (HFix s f) where
  free = HVar <$> newHRef Nothing
  isVar (HVar v) = Just v
  isVar _ = Nothing

instance (Unify s (f (HFix s f))) => Unify s (HFix s f) where
  (Other f1) `unify` (Other f2) = f1 `unify` f2
  _ `unify` _ = mzero

instance Show (f (HFix s f)) => Show (HFix s f) where
  show (HVar _) = "<var>"
  show (Other f) = show f

instance (Functor f, Traversable f) => HConvert s (Fix f) (HFix s f) where
  intoH (Fix f) = Other (fmap intoH f)
  fromH (HVar v) = variable v
  fromH (Other f) = do
    f' <- traverse fromH f
    return $ Fix f'

data HMaybe s a = HJust a
                | HNothing
                | HMVar (Var s (HMaybe s a))

instance (Unify s a, Free s a) => Unify s (HMaybe s a) where
  (HJust l) `unify` (HJust r) = l === r
  HNothing `unify` HNothing = return ()
  _ `unify` _ = mzero

instance Free s (HMaybe s a) where
  free = HMVar <$> newHRef Nothing
  isVar (HMVar v) = Just v
  isVar _ = Nothing

instance HConvert s a b => HConvert s (Maybe a) (HMaybe s b) where
  intoH (Just a) = HJust $ intoH a
  intoH Nothing = HNothing
  fromH (HJust a) = Just <$> fromH a
  fromH HNothing = return Nothing
  fromH (HMVar v) = variable v

instance (Show a) => Show (HMaybe s a) where
  show (HJust a) = "Just " ++ show a
  show HNothing = "Nothing"
  show (HMVar _) = "<var>"

data HTuple s l r = HTuple l r
                  | HTVar (Var s (HTuple s l r))

instance (Unify s l, Unify s r, Free s l, Free s r) => Unify s (HTuple s l r) where
  (HTuple l1 r1) `unify` (HTuple l2 r2) = l1 === l2 >> r1 === r2
  _ `unify` _ = mzero

instance Free s (HTuple s l r) where
  free = HTVar <$> newHRef Nothing
  isVar (HTVar v) = Just v
  isVar _ = Nothing

instance (HConvert s l l', HConvert s r r') => HConvert s (l, r) (HTuple s l' r') where
  intoH (l, r) = HTuple (intoH l) (intoH r)
  fromH (HTuple l r) = (,) <$> fromH l <*> fromH r
  fromH (HTVar v) = variable v

instance (Show l, Show r) => Show (HTuple s l r) where
  show (HTuple l r) = "(" ++ show l ++ ", " ++ show r ++ ")"
  show (HTVar _) = "<var>"

data HWrapper s a = Wrapped a
                  | HWVar (Var s (HWrapper s a))

instance Eq a => Unify s (HWrapper s a) where
  (Wrapped l) `unify` (Wrapped r) = guard $ l == r
  _ `unify` _ = mzero

instance Free s (HWrapper s a) where
  free = HWVar <$> newHRef Nothing
  isVar (HWVar v) = Just v
  isVar _ = Nothing

instance HConvert s a (HWrapper s a) where
  intoH = Wrapped
  fromH (Wrapped a) = return a
  fromH (HWVar v) = variable v

instance Show a => Show (HWrapper s a) where
  show (Wrapped a) = show a
  show (HWVar _) = "<var>"

type HChar s = HWrapper s Char
type HString s = HList s (HChar s)

instance HConvert s T.Text (HString s) where
  intoH t = intoH $ T.unpack t
  fromH s = T.pack <$> fromH s
